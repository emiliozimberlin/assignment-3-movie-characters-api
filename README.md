# Movie Character API
A ASP.NET Core Web API C#. 
## Table of Contents
- [Description](#Description)
- [Installation](#Installation)
- [Components](#Components)
- [Maintainer](#Maintainer)
- [Author](#Author)
- [License](#License)
## Description
This is an API that contains Movie, Character and Franchie classes. It has seeding and DTOs. 
Furthermore it has generic CRUD functions for all classes and some additional functions.
It also contains swagger documentation.

## Installation
Run ssms change the builder.DataSource to the name of your database. Then run it in Visual Studios, then press the play button.

## Components
This project contains three domain classes(Movie, Character, Franchise) wtih DTOs, Services, IServices, Profies and controllers.

## Maintainer
[@emiliozimberlin](https://gitlab.com/emiliozimberlin)

## Author
Emilio Zimberlin
## License
[MIT](https://choosealicense.com/licenses/mit/)
