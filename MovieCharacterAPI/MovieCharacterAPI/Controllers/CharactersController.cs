﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO;
using MovieCharacterAPI.Services;

namespace MovieCharacterAPI.Controllers
{
    /// <summary>
    /// Character controller.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets all characters for the database.
        /// </summary>
        /// <returns>A list of chracters</returns>
        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());

        }
        /// <summary>
        /// Gets a specific character from the database.
        /// </summary>
        /// <param name="id">character id</param>
        /// <returns>A Character</returns>
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterService.GetSpecificCharacterAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }
        /// <summary>
        /// Updatates a character in the database.
        /// </summary>
        /// <param name="id">character id</param>
        /// <param name="characterDTO">CharacterEditDTO object</param>
        /// <returns></returns>
        // PUT: api/Characters/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDTO)
        {
            if (id != characterDTO.Id)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(characterDTO);

            await _characterService.UpdateCharacterAsync(domainCharacter);

            return NoContent();
        }
        /// <summary>
        /// Adds a character in the database.
        /// </summary>
        /// <param name="characterDTO">CharacterCreateDTO object</param>
        /// <returns></returns>
        // POST: api/Characters
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO characterDTO)
        {
          
            Character domainCharacter = _mapper.Map<Character>(characterDTO);

            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);


            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, _mapper.Map<CharacterReadDTO>(domainCharacter));
        }
        /// <summary>
        /// Deletes a character in the database.
        /// </summary>
        /// <param name="id">character id</param>
        /// <returns></returns>
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();

        }
        /// <summary>
        /// Adds a character to a movie in the database.
        /// </summary>
        /// <param name="id">character id</param>
        /// <param name="movies">movie list</param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateCharacterMovie(int id, List<int> movies)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            try
            {
                await _characterService.UpdateCharacterMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movies.");
            }

            return NoContent();
        }

    }
}
