﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO;
using MovieCharacterAPI.Models.DTO.Franchise;
using MovieCharacterAPI.Models.DTO.Movie;
using MovieCharacterAPI.Services;

namespace MovieCharacterAPI.Controllers
{
    /// <summary>
    /// Franchise controller.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets all franchises from the database.
        /// </summary>
        /// <returns>A List of franchises</returns>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }
        /// <summary>
        /// Gets all moveis in a specific franchise from the database.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>A List of Movies</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetAllMoviesInAFranchiseAsync(id));
        }
        /// <summary>
        /// Gets all characters in a specific franchise from the database.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>A List of Movies</returns>
        [HttpGet("{id}/character")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetAllCharactersInAFranchiseAsync(id));
        }
        /// <summary>
        /// Gets specific franchise from the database.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>A franchise</returns>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }
        /// <summary>
        /// Updates franchise in the database.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <param name="franchiseDTO">FranchiseEditDTO object</param>
        /// <returns></returns>
        // PUT: api/Franchises/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDTO)
        {
            if (id != franchiseDTO.Id)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDTO);

            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }
        /// <summary>
        /// Adds a franchise to the database.
        /// </summary>
        /// <param name="franchiseDTO">FranchiseCreateDTO object</param>
        /// <returns>A franchise</returns>
        // POST: api/Franchises
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchiseDTO)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDTO);

            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);


            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }
        /// <summary>
        /// Deletes a franchise in the database.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns></returns>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Adds a franchise to a movie in the database.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <param name="movies">movie list</param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovie(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movies.");
            }

            return NoContent();
        }

    }
}
