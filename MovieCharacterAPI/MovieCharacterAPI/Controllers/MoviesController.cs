﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO;
using MovieCharacterAPI.Models.DTO.Movie;
using MovieCharacterAPI.Services;

namespace MovieCharacterAPI.Controllers
{
    /// <summary>
    /// Movie controller.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets all moveis in the database.
        /// </summary>
        /// <returns>A list of movies.</returns>
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }
        /// <summary>
        /// Gets all characters in a specific movie from the database.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns>A List of characters</returns>
        [HttpGet("{id}/character")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetAllCharactersInMovieAsync(id));
        }
        /// <summary>
        /// Gets a specific movie in the database.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns>movie object</returns>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetSpecificMovieAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }
        /// <summary>
        /// Updates a movie in the database.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <param name="movieDTO">MovieEditDTO object</param>
        /// <returns></returns>
        // PUT: api/Movies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieDTO)
        {
            if (id != movieDTO.Id)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(movieDTO);

            await _movieService.UpdateMovieAsync(domainMovie);
            return NoContent();
        }
        /// <summary>
        /// adds a movie into the database.
        /// </summary>
        /// <param name="movieDTO">MovieCreateDTO object</param>
        /// <returns>movie object</returns>
        // POST: api/Movies
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO movieDTO)
        {
            Movie domainMovie = _mapper.Map<Movie>(movieDTO);

            domainMovie = await _movieService.AddMovieAsync(domainMovie);


            return CreatedAtAction("GetMovie", new { id = domainMovie.Id }, _mapper.Map<MovieReadDTO>(domainMovie));
        }
        /// <summary>
        /// Deletes a movie in the database.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns></returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Adds a movie to a character in the database.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <param name="characters">character list</param>
        /// <returns></returns>
        [HttpPut("{id}/character")]
        public async Task<IActionResult> UpdateMovieCharacter(int id, List<int> characters)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieCharacterAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movies.");
            }

            return NoContent();
        }

    }
}
