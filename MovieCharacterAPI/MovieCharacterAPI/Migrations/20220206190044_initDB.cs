﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class initDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Year = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false),
                    MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Male", "Tony Stark", "https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/" },
                    { 2, "Spider Man", "Male", "Peter Parker", "https://www.imdb.com/title/tt10872600/mediaviewer/rm3936939521/" },
                    { 3, "Superman ", "Male", "Clark Kent", "https://www.imdb.com/title/tt0770828/mediaviewer/rm2035131904/" },
                    { 4, "Wonder Woman", "Female", "Diana Prince", "https://www.imdb.com/title/tt0451279/mediaviewer/rm1317819648/" },
                    { 5, "Mr. Raptor", "Male", "Owen", "https://www.imdb.com/title/tt0369610/mediaviewer/rm1702622464/?ft0=name&fv0=nm0695435&ft1=image_type&fv1=still_frame" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Is an American film and television production company that is a subsidiary of Walt Disney Studios, a division of The Walt Disney Company.", "Marvel studios" },
                    { 2, "DC Films is an American film studio that is a subsidiary of Warner Bros. through the Warner Bros.", "DC Films" },
                    { 3, "Jurassic Park, later also referred to as Jurassic World, is an American science fiction media franchise centered on a disastrous attempt to create a theme park of cloned dinosaurs.", "Jurassic Park" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Name", "Picture", "Trailer", "Year" },
                values: new object[,]
                {
                    { 3, "Russo brothers", 1, "Action", "Avengers: Infinity War", "https://www.imdb.com/title/tt4154756/mediaviewer/rm4044245504/", "https://www.youtube.com/watch?v=6ZfuNTqbHE8&ab_channel=MarvelEntertainment", 2018 },
                    { 4, "Jon Favreau", 1, "Action", "Iron Man", "https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/", "https://www.youtube.com/watch?v=8ugaeA-nMTc&ab_channel=MovieclipsClassicTrailers", 2008 },
                    { 1, "Zack Snyder", 2, "Action", "Justice League", "https://www.imdb.com/title/tt0974015/mediaviewer/rm1061640448/", "https://www.youtube.com/watch?v=3cxixDgHUYw&ab_channel=WarnerBros.Pictures", 2017 },
                    { 2, "Zack Snyder", 2, "Action", "Batman v Superman: Dawn of Justice", "https://www.imdb.com/title/tt2975590/mediaviewer/rm2302675456/", "https://www.youtube.com/watch?v=0WWzgGyAH6Y&ab_channel=WarnerBros.Pictures", 2016 },
                    { 5, "Colin Trevorrow", 3, "Action", "Jurassic World", "https://www.imdb.com/title/tt0369610/mediaviewer/rm2047901184/", "https://www.youtube.com/watch?v=RFinNxS5KN4&ab_channel=UniversalPictures", 2015 }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[] { 1, 2 });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MovieId",
                table: "CharacterMovie",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
