﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Movie;
using System.Linq;

namespace MovieCharacterAPI.Profiles
{
    /// <summary>
    /// Profile class of Movie
    /// </summary>
    public class MovieProflie : Profile
    {
        public MovieProflie()
        {
            // Movie->MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                // Turning related Characters into int arrays
                .ForMember(cdto => cdto.Characters, opt => opt
                .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()));

            // MovieCreateDTO->Movie
            CreateMap<MovieCreateDTO, Movie>();
            // MovieEditDTO->Movie
            CreateMap<MovieEditDTO, Movie>();
        }
    }
}
