﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieCharacterAPI.Profiles
{
    /// <summary>
    /// Profile class of Character
    /// </summary>
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character->CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                // Turning related Movies into int arrays
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            // CharacterCreateDTO->Character
            CreateMap<CharacterCreateDTO, Character>();
            // CharacterEditDTO->Character
            CreateMap<CharacterEditDTO, Character>();


        }
    }
}
