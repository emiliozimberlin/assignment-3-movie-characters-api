﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Franchise;
using System.Linq;

namespace MovieCharacterAPI.Profiles
{
    /// <summary>
    /// Profile class of Franchise
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Franchise->FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                // Turning related movies into int arrays
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            // FranchiseCreateDTO->Franchise
            CreateMap<FranchiseCreateDTO, Franchise>();
            // FranchiseEditDTO->Franchise
            CreateMap<FranchiseEditDTO, Franchise>();

        }
    }
}
