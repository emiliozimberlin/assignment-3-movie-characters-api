﻿using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services
{
    /// <summary>
    /// A interface for FranchiseService.
    /// </summary>
    public interface IFranchiseService
    {
        /// <summary>
        /// Gets all franchises.
        /// </summary>
        /// <returns>A List of franchises</returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        /// <summary>
        /// Gets specific franchise.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>A franchise</returns>
        public Task<Franchise> GetSpecificFranchiseAsync(int id);
        /// <summary>
        /// Adds a franchise.
        /// </summary>
        /// <param name="franchise">franchise object</param>
        /// <returns>A franchise</returns>
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        /// <summary>
        /// Updates franchise.
        /// </summary>
        /// <param name="franchise">franchise object</param>
        /// <returns></returns>
        public Task UpdateFranchiseAsync(Franchise franchise);
        /// <summary>
        /// Adds a franchise to a movie.
        /// </summary>
        /// <param name="franchiseId">franchise id</param>
        /// <param name="movies">movie list</param>
        /// <returns></returns>
        public Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies);
        /// <summary>
        /// Deletes a franchise.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns></returns>
        public Task DeleteFranchiseAsync(int id);
        /// <summary>
        /// Checks if a franchise exists.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>True if it exists, else false</returns>
        public bool FranchiseExists(int id);
        /// <summary>
        /// Gets all movies from a specific franchise.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>A list of movies</returns>
        public Task<IEnumerable<Movie>> GetAllMoviesInAFranchiseAsync(int id);
        /// <summary>
        /// Gets all characters in a specific franchise.
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>A List of Movies</returns>
        public Task<IEnumerable<Character>> GetAllCharactersInAFranchiseAsync(int id);

    }
}
