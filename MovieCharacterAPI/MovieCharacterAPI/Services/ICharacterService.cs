﻿using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services
{
    /// <summary>
    /// A interface for CharacterService.
    /// </summary>
    public interface ICharacterService
    {
        /// <summary>
        /// Gets a list of all Characters.
        /// </summary>
        /// <returns>List of Characters</returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();
        /// <summary>
        /// Gets a specific character.
        /// </summary>
        /// <param name="id">character id</param>
        /// <returns>A Character</returns>
        public Task<Character> GetSpecificCharacterAsync(int id);
        /// <summary>
        /// Adds a character.
        /// </summary>
        /// <param name="character">A character object</param>
        /// <returns>A character</returns>
        public Task<Character> AddCharacterAsync(Character character);
        /// <summary>
        /// Updatates a character.
        /// </summary>
        /// <param name="character">a Character</param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);
        /// <summary>
        /// Adds a character to a movie.
        /// </summary>
        /// <param name="characterId">character id</param>
        /// <param name="movies">movie list</param>
        /// <returns></returns>
        public Task UpdateCharacterMoviesAsync(int characterId, List<int> movies);
        /// <summary>
        /// Deletes a character.
        /// </summary>
        /// <param name="id">character id</param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int id);
        /// <summary>
        /// Checks if a character exixts.
        /// </summary>
        /// <param name="id">character id</param>
        /// <returns>True if it exists, else false</returns>
        public bool CharacterExists(int id);

    }
}
