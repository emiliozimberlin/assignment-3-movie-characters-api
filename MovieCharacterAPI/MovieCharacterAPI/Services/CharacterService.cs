﻿using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;

using System.Linq;



namespace MovieCharacterAPI.Services
{
    /// <summary>
    /// CharacterService class
    /// </summary>
    public class CharacterService : ICharacterService
    {
        private readonly MovieDbContext _context;

        public CharacterService(MovieDbContext context)
        {
            _context = context;
        }
        /// <inheritdoc/>
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;

        }
        /// <inheritdoc/>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
        /// <inheritdoc/>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

        }
        /// <inheritdoc/>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .ToListAsync();

        }
        /// <inheritdoc/>
        public async Task<Character> GetSpecificCharacterAsync(int id)
        {
            return await _context.Characters.FindAsync(id);
        }
        /// <inheritdoc/>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }
        /// <inheritdoc/>
        public async Task UpdateCharacterMoviesAsync(int characterId, List<int> movies)
        {
            Character CharacterToUpdateMovies = await _context.Characters
                .Include(m => m.Movies)
                .Where(c => c.Id == characterId)
                .FirstAsync();

            // Loop through certificates, try and assign to coach
            // Trying to see if there is a nicer way of doing this, dont like the multiple calls
            List<Movie> newMovies = new();
            foreach (int movieId in movies)
            {
                Movie newMovie = await _context.Movies.FindAsync(movieId);
                if (newMovie == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                newMovies.Add(newMovie);
            }
            CharacterToUpdateMovies.Movies = newMovies;
            await _context.SaveChangesAsync();

        }
    }
}
