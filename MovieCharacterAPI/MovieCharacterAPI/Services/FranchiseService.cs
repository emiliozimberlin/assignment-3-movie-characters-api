﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services
{
    /// <summary>
    /// FranchiseService class
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }
        /// <inheritdoc/>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }
        /// <inheritdoc/>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }
        /// <inheritdoc/>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
        /// <inheritdoc/>
        public async Task<IEnumerable<Character>> GetAllCharactersInAFranchiseAsync(int id)
        {
            return await _context.Movies
                .Where(f => f.FranchiseId == id)
                .SelectMany(c => c.Characters)
                .Distinct().ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
                .Include(m => m.Movies)
                .ToListAsync();

        }
        /// <inheritdoc/>
        public async Task<IEnumerable<Movie>> GetAllMoviesInAFranchiseAsync(int id)
        {
            return await _context.Movies
                 .Where(f => f.FranchiseId == id)
                 .ToListAsync();

        }

        /// <inheritdoc/>
        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }
        /// <inheritdoc/>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <inheritdoc/>
        public async Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies)
        {
            Franchise FranchiseToUpdateMovies = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == franchiseId)
                .FirstAsync();

            // Loop through certificates, try and assign to coach
            // Trying to see if there is a nicer way of doing this, dont like the multiple calls
            List<Movie> newMovies = new();
            foreach (int movieId in movies)
            {
                Movie newMovie = await _context.Movies.FindAsync(movieId);
                if (newMovie == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                newMovies.Add(newMovie);
            }
            FranchiseToUpdateMovies.Movies = newMovies;
            await _context.SaveChangesAsync();
        }
    }
}
