﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services
{
    /// <summary>
    /// MovieService class
    /// </summary>
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context)
        {
            _context = context;
        }
        /// <inheritdoc/>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }
        /// <inheritdoc/>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        /// <inheritdoc/>
        public async Task<IEnumerable<Character>> GetAllCharactersInMovieAsync(int id)
        {
            return  await _context.Movies
                .Where(m => m.Id == id)
                .Include(c => c.Characters)
                .SelectMany(c => c.Characters)  
                .ToListAsync();

        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(c => c.Characters)
                .ToListAsync();
        }
        /// <inheritdoc/>
        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }
        /// <inheritdoc/>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
        /// <inheritdoc/>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <inheritdoc/>
        public async Task UpdateMovieCharacterAsync(int movieId, List<int> characters)
        {
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.Id == movieId)
                .FirstAsync();

            // Loop through certificates, try and assign to coach
            // Trying to see if there is a nicer way of doing this, dont like the multiple calls
            List<Character> charas = new();
            foreach (int charId in characters)
            {
                Character chara = await _context.Characters.FindAsync(charId);
                if (chara == null)
                    // Record doesnt exist: https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-4.0/ms229021(v=vs.100)?redirectedfrom=MSDN
                    throw new KeyNotFoundException();
                charas.Add(chara);
            }
            movieToUpdateCharacters.Characters = charas;
            await _context.SaveChangesAsync();

        }
    }
}
