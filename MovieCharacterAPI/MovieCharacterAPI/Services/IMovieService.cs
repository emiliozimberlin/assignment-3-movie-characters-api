﻿using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services
{
    /// <summary>
    /// A interface for MovieService.
    /// </summary>
    public interface IMovieService
    {
        /// <summary>
        /// Gets all moveis.
        /// </summary>
        /// <returns>A list of movies.</returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        /// <summary>
        /// Gets a specific movie.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns>movie object</returns>
        public Task<Movie> GetSpecificMovieAsync(int id);
        /// <summary>
        /// adds a movie.
        /// </summary>
        /// <param name="movie">movie object</param>
        /// <returns>movie object</returns>
        public Task<Movie> AddMovieAsync(Movie movie);
        /// <summary>
        /// Updates a movie.
        /// </summary>
        /// <param name="movie">movie object</param>
        /// <returns></returns>
        public Task UpdateMovieAsync(Movie movie);
        /// <summary>
        /// Adds a movie to a character.
        /// </summary>
        /// <param name="movieId">movie id</param>
        /// <param name="characters">character list</param>
        /// <returns></returns>
        public Task UpdateMovieCharacterAsync(int movieId, List<int> characters);
        /// <summary>
        /// Deletes a movie.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns></returns>
        public Task DeleteMovieAsync(int id);
        /// <summary>
        /// Checks if a movie exists.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns>True if it exists, else false</returns>
        public bool MovieExists(int id);
        /// <summary>
        /// Gets all characters in a specific movie.
        /// </summary>
        /// <param name="id">movie id</param>
        /// <returns>A List of characters</returns>
        public Task<IEnumerable<Character>> GetAllCharactersInMovieAsync(int id);
    }
}
