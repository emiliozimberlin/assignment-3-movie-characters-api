﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MovieCharacterAPI.Models
{
    public class MovieDbContext : DbContext
    {
        // Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Seed data
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Marvel studios", Description = "Is an American film and television production company that is a subsidiary of Walt Disney Studios, a division of The Walt Disney Company." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "DC Films", Description = "DC Films is an American film studio that is a subsidiary of Warner Bros. through the Warner Bros." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Jurassic Park", Description = "Jurassic Park, later also referred to as Jurassic World, is an American science fiction media franchise centered on a disastrous attempt to create a theme park of cloned dinosaurs." });

            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, Name = "Tony Stark", Alias = "Iron Man", Gender = "Male", Picture = "https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, Name = "Peter Parker", Alias = "Spider Man", Gender = "Male", Picture = "https://www.imdb.com/title/tt10872600/mediaviewer/rm3936939521/" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 3, Name = "Clark Kent", Alias = "Superman ", Gender = "Male", Picture = "https://www.imdb.com/title/tt0770828/mediaviewer/rm2035131904/" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 4, Name = "Diana Prince", Alias = "Wonder Woman", Gender = "Female", Picture = "https://www.imdb.com/title/tt0451279/mediaviewer/rm1317819648/" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 5, Name = "Owen", Alias = "Mr. Raptor", Gender = "Male", Picture = "https://www.imdb.com/title/tt0369610/mediaviewer/rm1702622464/?ft0=name&fv0=nm0695435&ft1=image_type&fv1=still_frame" });


            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, Name = "Justice League", FranchiseId = 2, Genre = "Action", Year = 2017, Director = "Zack Snyder", Picture = "https://www.imdb.com/title/tt0974015/mediaviewer/rm1061640448/", Trailer = "https://www.youtube.com/watch?v=3cxixDgHUYw&ab_channel=WarnerBros.Pictures" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, Name = "Batman v Superman: Dawn of Justice", FranchiseId = 2, Genre = "Action", Year = 2016, Director = "Zack Snyder", Picture = "https://www.imdb.com/title/tt2975590/mediaviewer/rm2302675456/", Trailer = "https://www.youtube.com/watch?v=0WWzgGyAH6Y&ab_channel=WarnerBros.Pictures" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, Name = "Avengers: Infinity War", FranchiseId = 1, Genre = "Action", Year = 2018, Director = "Russo brothers", Picture = "https://www.imdb.com/title/tt4154756/mediaviewer/rm4044245504/", Trailer = "https://www.youtube.com/watch?v=6ZfuNTqbHE8&ab_channel=MarvelEntertainment" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, Name = "Iron Man", FranchiseId = 1, Genre = "Action", Year = 2008, Director = "Jon Favreau", Picture = "https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/", Trailer = "https://www.youtube.com/watch?v=8ugaeA-nMTc&ab_channel=MovieclipsClassicTrailers" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 5, Name = "Jurassic World", FranchiseId = 3, Genre = "Action", Year = 2015, Director = "Colin Trevorrow", Picture = "https://www.imdb.com/title/tt0369610/mediaviewer/rm2047901184/", Trailer = "https://www.youtube.com/watch?v=RFinNxS5KN4&ab_channel=UniversalPictures" });



            // Seed m2m coach-certification. Need to define m2m and access linking table
            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 }
                        );
                    });


        }
    }
}
