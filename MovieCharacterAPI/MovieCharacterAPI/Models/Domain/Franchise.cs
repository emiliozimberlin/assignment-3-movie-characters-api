﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    /// <summary>
    /// Domain class of Franchise
    /// </summary>
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }
        public ICollection<Movie> Movies { get; set; }

    }
}
