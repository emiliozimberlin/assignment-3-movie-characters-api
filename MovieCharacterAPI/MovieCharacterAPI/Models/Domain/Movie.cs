﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    /// <summary>
    /// Domain class of Movie
    /// </summary>
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
 
        [MaxLength(50)]
        public string Genre { get; set; }
        public int Year { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(2048)]
        public string Picture { get; set; }
        [MaxLength(2048)]
        public string Trailer { get; set; }

        public ICollection<Character> Characters { get; set; }
        public int FranchiseId { get; set; }
        public Franchise Franchises { get; set; }
    }
}
