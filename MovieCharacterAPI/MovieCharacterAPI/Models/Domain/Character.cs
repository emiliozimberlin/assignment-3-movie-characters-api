﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    /// <summary>
    /// Domain class of Character
    /// </summary>
    public class Character
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [MaxLength(2048)]
        public string Picture { get; set; }
        public ICollection<Movie> Movies { get; set; }

    }
}
