﻿using System.Collections.Generic;

namespace MovieCharacterAPI.Models.DTO.Movie
{
    /// <summary>
    /// DTO read class of Movie
    /// </summary>
    public class MovieReadDTO
    {
        public int Id { get; set; } 
        public string Name { get; set; }

        public string Genre { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }

        public ICollection<int> Characters { get; set; }
        public int FranchiseId { get; set; }
        public int Franchises { get; set; }
    }
}
