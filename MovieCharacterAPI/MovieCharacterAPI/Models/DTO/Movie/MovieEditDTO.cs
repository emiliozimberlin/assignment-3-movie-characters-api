﻿namespace MovieCharacterAPI.Models.DTO.Movie
{
    /// <summary>
    /// DTO edit class of Movie
    /// </summary>
    public class MovieEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Genre { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }

    }
}
