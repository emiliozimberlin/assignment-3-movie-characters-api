﻿namespace MovieCharacterAPI.Models.DTO.Movie
{
    /// <summary>
    /// DTO create class of Movie
    /// </summary>
    public class MovieCreateDTO
    {
        public string Name { get; set; }

        public string Genre { get; set; }
        public int Year { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }

    }
}
