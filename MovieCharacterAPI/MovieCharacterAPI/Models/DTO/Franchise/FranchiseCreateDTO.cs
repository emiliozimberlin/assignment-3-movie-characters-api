﻿using System.Collections.Generic;

namespace MovieCharacterAPI.Models.DTO.Franchise
{
    /// <summary>
    /// DTO create class of Franchise
    /// </summary>
    public class FranchiseCreateDTO
    {
 
        public string Name { get; set; }
        public string Description { get; set; }
    
    }
}
