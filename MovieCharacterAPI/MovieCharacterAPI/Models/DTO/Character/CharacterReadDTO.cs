﻿using System.Collections.Generic;

namespace MovieCharacterAPI.Models.DTO
{
    /// <summary>
    /// DTO read class of Character
    /// </summary>
    public class CharacterReadDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Alias { get; set; }

        public string Gender { get; set; }
 
        public string Picture { get; set; }
        public ICollection<int> Movies { get; set; }
    }
}
