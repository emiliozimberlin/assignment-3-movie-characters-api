﻿namespace MovieCharacterAPI.Models.DTO
{
    /// <summary>
    /// DTO edit class of Character
    /// </summary>
    public class CharacterEditDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Alias { get; set; }

        public string Gender { get; set; }

        public string Picture { get; set; }

    }
}
