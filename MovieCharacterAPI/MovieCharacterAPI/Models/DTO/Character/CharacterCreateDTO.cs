﻿namespace MovieCharacterAPI.Models.DTO
{
    /// <summary>
    /// DTO create class of Character
    /// </summary>
    public class CharacterCreateDTO
    {
        
        public string Name { get; set; }

        public string Alias { get; set; }

        public string Gender { get; set; }

        public string Picture { get; set; }
    }
}
